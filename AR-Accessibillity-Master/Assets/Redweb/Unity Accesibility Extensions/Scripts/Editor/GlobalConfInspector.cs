using UnityEngine; 
using System.Collections;
using UnityEditor; 

[CustomEditor(typeof(GlobalConf))]
public class GlobalConfInspector : Editor {
    GlobalConf globConf;
    [SerializeField]
    bool AccStatus;
    public override void OnInspectorGUI() {
        // Setting up definitions for things that will need to be referenced later private void OnAnimatorIK(int layerIndex) {
        // such as AccessibilityStatus scripts 
         globConf = (GlobalConf)target;
        GUILayout.Label("Unity Accessibility Extensions - Global Configuration ", EditorStyles.largeLabel);
        EditorGUILayout.HelpBox("The global configuration tool allows you to manage all aspects of the Accessibility Extensions, configure debugging settings and view statuses of features such as Debug Mode",MessageType.Info);
        StatusIndicators();
    }
    private void StatusIndicators()
    {
        GUIStyle richTxt = new GUIStyle(EditorStyles.wordWrappedLabel)
        {
            richText = true
        };
        string DbgString;
        string AccessString;
        if (globConf.DebugMode)
        {
            DbgString = ("Debug mode is <b><color=lime>enabled</color></b>");
        }
        else { DbgString = ("Debug mode is <b><color=red>disabled</color></b>"); }
        GUILayout.Label("Debugging Status", EditorStyles.boldLabel);
        GUILayout.Label(DbgString, richTxt);
        GUILayout.Label("Accessibility Service Status", EditorStyles.boldLabel);
        if (GlobalVariables.EditorAccessStatus)
        {
            AccessString = ("Accessibility services are <b><color=lime>enabled</color></b>");
        }
        else { AccessString = ("Accessibility services are <b><color=red>disabled</color></b>"); }
        GUILayout.Label(AccessString, richTxt);
        EditorGUILayout.HelpBox("By default, in the Unity editor, the accessibility services are always enabled. ", MessageType.Info);
        // TODO: Change this behaviour so that it is actually toggle-able. 

    }
}
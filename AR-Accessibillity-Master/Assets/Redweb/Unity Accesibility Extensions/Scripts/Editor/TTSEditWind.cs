﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TextSpeech;

[CustomEditor(typeof(TextToSpeech))]
public class TTSEdit : Editor
{
    TextToSpeech textToSpeech;
    float rate;
    float pitch;
    private void Awake()
    {
      
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        serializedObject.FindProperty("pitch").floatValue =  EditorGUILayout.Slider(serializedObject.FindProperty("pitch").floatValue, 0, 5);
        serializedObject.FindProperty("rate").floatValue =  EditorGUILayout.Slider(serializedObject.FindProperty("rate").floatValue, 0, 5);
        serializedObject.ApplyModifiedProperties();
    }
    private void OnValidate()
    {
      
    }
}

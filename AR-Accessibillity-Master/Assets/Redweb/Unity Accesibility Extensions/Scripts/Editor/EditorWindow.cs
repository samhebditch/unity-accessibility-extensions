﻿using UnityEngine;
using UnityEditor;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using UnityEngine.AI;
using UnityScript.Steps;

public class EditWindow : EditorWindow
{
    // These are used later on for the debugging options
    bool ConfDbg;
    GameObject confObj;
    bool nxtStpCofn;
    bool confSetup;
    bool customCamTag = true;
    string camTag;
    bool renameCameraTag = false;
    GameObject cameraObj;

    [MenuItem("Unity Accessibility Extensions/Setup")]
    static void Setup()
    {
        GetWindow<EditWindow>("U.A.E - Setup");
        
    }

    
    

    private void OnGUI()
    {
        // Custom editor styles are used to provide stylised headers within the GUI. 
        GUIStyle wrapRichTxt = new GUIStyle(EditorStyles.wordWrappedLabel)
        {
            richText = true
        };
        GUILayout.Label("Unity Accessibility Extensions - Setup", EditorStyles.largeLabel);
        GUILayout.Label("This is the main set up tool that is used to add in the various GameObjects and scripts to your scene automatically. This will work for any scene or camera set-up that follows Unity's default naming conventions, however, will probably require tweaking for anything more bespoke, or multi-camera setups.", EditorStyles.wordWrappedLabel);
        GUILayout.Space(5f);
        // Adding in the GameObject which contains the scripts for global configuration, and assigning a tag that can be referenced later on if needed. 
        GUILayout.Label("<b> Step 1: Add Global Configuration object </b>", wrapRichTxt);
        EditorGUILayout.HelpBox("This step adds in a GameObject to your scene which contains the neccessary components for configuration and overall functionality", MessageType.Info);
        if (GUILayout.Button("Add Global Configuration object"))
        {
            TagSetup("GlobalConf");
            TagSetup("TTS");
            UnityEngine.Object prefab = AssetDatabase.LoadAssetAtPath("Assets/Redweb/Unity Accesibility Extensions/Prefabs/Global Configuration.prefab", typeof(GameObject));
            GameObject clone = Instantiate(prefab) as GameObject;
            Debug.Log("Instantiated the prefab");
            clone.name = "Global Configuration";
            clone.tag = "GlobalConf";
            GameObject tts = GameObject.Find("TextToSpeech");
            tts.tag = "TTS";
            confSetup = true;
        }
        // Setting up the camera, creating two tags, instantiating a primitive that will be the source of the raycast and then parenting and hiding it
        //from the camera. Adding tags to these objects, so that they too, can be referenced later on. 
        GUILayout.Label("<b>Step 2: Configure the Camera</b>",wrapRichTxt);
        EditorGUILayout.HelpBox("This step assigns a new tag to your scene camera, replacing the default one, and then adds a GameObject used for Raycasting as a child of the camera. ", MessageType.Info);
        if(GUILayout.Button("Setup Camera")){
            TagSetup("ScnCam");
            TagSetup("CastingCube");
            Debug.Log("Setting up the camera");
            Camera cam = FindObjectOfType<Camera>();

            // Breaking out the camera renaming functionality
            if(customCamTag && !renameCameraTag)
            {
                cam.tag = "ScnCam";
            }
            else if (customCamTag && renameCameraTag)
            {
                cam.tag = camTag;
            }
            // ending the breakout here, for neatness.
           GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = cam.transform.position;
            cube.transform.SetParent(cam.transform);
            cube.name = "Casting Cube";
            cube.GetComponent<MeshRenderer>().enabled = false;
            cube.GetComponent<BoxCollider>().enabled = false; 
            cube.tag = "CastingCube";
            nxtStpCofn = true;
            AddScript();
        }
        if (confSetup)
        {
            confObj = GameObject.FindGameObjectWithTag("GlobalConf");
        }
        GUILayout.Space(5f);
        StatusChecker();
        DebugMode();
        CustomCamTag();
    }
    private void AddScript()
    {
        if (nxtStpCofn)
        {
            if (customCamTag && !renameCameraTag)
            {
                 cameraObj = GameObject.FindGameObjectWithTag("ScnCam");
            }
            else if (customCamTag && renameCameraTag)
            {
                cameraObj = GameObject.FindGameObjectWithTag(camTag);
            }
            else if (!customCamTag && !renameCameraTag)
            {
                cameraObj = GameObject.FindGameObjectWithTag("MainCamera");
            }
            //GameObject cameraObj = GameObject.FindGameObjectWithTag("ScnCam");
            cameraObj.AddComponent<EventHandler>();
            cameraObj.AddComponent<RotFeedbk>();
        }
    }
    // Checking the status of each of the objects/tags and reporting back to the user 
    private void StatusChecker()
    {
        // Styling functions
        GUIStyle richTxt = new GUIStyle(EditorStyles.wordWrappedLabel)
        {
            richText = true
        };
        // Logic to check status 
        String camStatus = String.Empty;
        String castCubeStatus = String.Empty;
        String globConfStatus = String.Empty;
        if (nxtStpCofn)
        {
            if (customCamTag && !renameCameraTag)
            {
                if (GameObject.FindGameObjectWithTag("ScnCam"))
                {
                    camStatus = "<color=lime>configured successfully</color>";
                }
            }
            else if (customCamTag && renameCameraTag)
            {
                if (GameObject.FindGameObjectWithTag(camTag))
                {
                    camStatus = "<color=lime>configured successfully</color>";
                }
            }
            else if (!customCamTag && !renameCameraTag)
            {
                if (GameObject.FindGameObjectWithTag("MainCamera"))
                {
                    camStatus = "<color=lime>configured successfully</color>";
                }
            }
            else
            {
                camStatus = "<color=red>not configured</color>";
            }
            if (GameObject.FindGameObjectWithTag("CastingCube"))
            {
                castCubeStatus = "<color=lime>configured successfully</color>";
            }
            else
            {
                castCubeStatus = "<color=red>not configured</color>";
            }
        }
        if (confSetup)
        {
            if (GameObject.FindGameObjectWithTag("GlobalConf"))
            {
                globConfStatus = "<color=lime>configured successfully</color>";
            }
            else
            {
                globConfStatus = "<color=red>not configured</color>";
            }
        }
        // Presenting this in the UI 
        GUILayout.Label("Status Checker",EditorStyles.boldLabel);
        GUILayout.Label("Camera: " + camStatus,richTxt);
        GUILayout.Label("Casting Cube: " + castCubeStatus, richTxt);
        GUILayout.Label("Global Configuration: "+globConfStatus, richTxt);
     
    }
    // This is used to programaticaly add in tags when needed, you provide it with a string, and the tag gets added to the tags list. 
    // Useful for finding objects even if the user renames them, and adding in tags to a vanilla scene/project. 
    private void TagSetup(String tagString)
    {
        String tagStr = tagString;
        SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty tagsProp = tagManager.FindProperty("tags");

        bool found = false;
        for (int i = 0; i < tagsProp.arraySize; i++)
        {
            SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
            if (t.stringValue.Equals(tagStr)) { found = true; break; }
        }
        if (!found)
        {
            tagsProp.InsertArrayElementAtIndex(0);
            SerializedProperty n = tagsProp.GetArrayElementAtIndex(0);
            n.stringValue = tagStr;
            tagManager.ApplyModifiedPropertiesWithoutUndo();
        }
    }
    // Debug mode toggle and warning. Warns the user that there might be dragons ahead, if they enable this, 
    // or there might be untested features. (Although ideally these would be kept on another branch somehwere).
    private void DebugMode()
    {

        GUILayout.Label("Debugging settings",EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("WARNING: There might be dragons ahead! Enabling the debugging features could break functionality, or impact usability of the extensions", MessageType.Warning);
        ConfDbg = EditorGUILayout.Toggle("Enable Debug Mode", ConfDbg);
        if (confObj != null)
        {
            if (confSetup)
            {
                if (confObj.activeInHierarchy)
                {
                    if (ConfDbg == true)
                    {
                        confObj.GetComponent<GlobalConf>().SetDbugTrue();
                    }
                    else
                    {
                        confObj.GetComponent<GlobalConf>().SetDbugFalse();
                    }
                }
            }
        else    
        {
            
        }
        }
        
    }
    private void CustomCamTag()
    {
        string LabelTag = "on";
        if (!customCamTag)
        {
            LabelTag = "disabled";
        }
        else
        {
            LabelTag = "enabled";
        }
        GUILayout.Label("Custom Camera Tag", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("If you're using a custom camera tag for the camera, you can define it here, and choose whether you want the camera to be retagged, or remain the same.", MessageType.Info);
        customCamTag = EditorGUILayout.ToggleLeft("Camera re-tagging is " + LabelTag, customCamTag);
        renameCameraTag = EditorGUILayout.ToggleLeft("Use a custom camera tag", renameCameraTag);
        EditorGUI.BeginDisabledGroup(!renameCameraTag);
        camTag = EditorGUILayout.TextField("Custom Camera Tag: ", camTag);
        EditorGUI.EndDisabledGroup();
    }
}

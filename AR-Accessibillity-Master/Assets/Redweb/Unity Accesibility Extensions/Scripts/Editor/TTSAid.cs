﻿using UnityEngine;
using UnityEditor;
// TTS Aid: 
// This script is designed to aid developers with the text-to-speech feedback 
// simulating it within an Editor window, so that development without a device is 
// easier. 

public class TTSAid : EditorWindow
{
    string RCStatus;
    GUIStyle custStyle;
    [MenuItem("Unity Accessibility Extensions/TTS Tool")]

    static void TTSTool()
    {
        GetWindow<TTSAid>("U.A.E - TTS Aid");
    }
private void OnGUI()
    {
    if(GameObject.FindGameObjectWithTag("GlobalConf") != null)
    {
        RCasting RCScript = GameObject.FindGameObjectWithTag("GlobalConf").GetComponent<RCasting>();
    }

        // Setting up custom styles for the UI elements as needed
        custStyle = new GUIStyle(EditorStyles.wordWrappedLabel)
        {
            fontSize = 80,
            fontStyle = FontStyle.Bold,
            alignment = (UnityEngine.TextAnchor)TextAlignment.Center,
            wordWrap = true,
            richText = true
            
        };
        // Setting up the rest of the UI
        GUILayout.Label("Unity Accessibility Extensions - TTS Aid", EditorStyles.largeLabel);
        GUILayout.Label("This tool is designed to allow developers to test out the text-to-speech feedback within the Unity Editor, It is presented via a large on screen label", EditorStyles.helpBox);
        GUILayout.Label("Raycast Status", EditorStyles.boldLabel);
        GUILayout.Label(RCStatus,custStyle);
        GUILayout.Label("Raycast Information", EditorStyles.boldLabel);
        GUILayout.Label(GlobalVariables.RaycastStatus(),EditorStyles.wordWrappedLabel);
        GUILayout.Label("Item Description", EditorStyles.boldLabel);
        GUILayout.Label(GlobalVariables.ItemDescription(), EditorStyles.wordWrappedLabel);
        RCStatusUpdate();
    }
    private void RCStatusUpdate()
    {
        GameObject GlobalConfObj = new GameObject("GlobalConfObj");
        GlobalConfObj.hideFlags = HideFlags.HideInHierarchy;
        if(GameObject.FindGameObjectWithTag("GlobalConf"))
        {
                 GlobalConfObj = GameObject.FindGameObjectWithTag("GlobalConf");
        }
        RCasting RCScript = GlobalConfObj.GetComponent<RCasting>();
        if (Application.isPlaying)
        {
            if (RCScript.HitStatus)
            {
                RCStatus = "<color=lime>Hit!</color>";
            }
            else if (!RCScript.HitStatus)
            {
                RCStatus = "<color=red>No hit!</color>";
            }
        }
        else if(Application.isEditor)
        {
            RCStatus = "N/A";
        }
    }
    
    
    
}


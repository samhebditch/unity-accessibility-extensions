﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EventDataStruct;
using Priority_Queue;


public class RotFeedbk : MonoBehaviour
{
    EventData lastEventData;
    string prevEventData;
    public EventData eventData;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        eventData = new EventData(2, AngleToClockFace((int)this.gameObject.transform.rotation.eulerAngles.y));
        if (GlobalVariables.AccessStatus)
        {
            if (!string.Equals(lastEventData.data, eventData.data))
            {
                GameObject.FindGameObjectWithTag("ScnCam").SendMessage("Eventhandler", eventData);
            }
        }
         lastEventData = eventData;
    }
    public static string AngleToClockFace(int angle)
    {
        // Putting the string that gets passed to the TTS system here 
        string TexttoSpeak;
        TexttoSpeak = "You are currently at: ";
        // Making sure the val string is empty before we begin 
        string val = string.Empty;
        // Switch case for the angles, this is the same as included in GlobalVariables, with the addition of the texttospeak string, which is a bit of intro text... 
        switch (angle)
        {
            case int n when (n <= 30):
                val = TexttoSpeak + "12'o'clock"; 
                break;
            case int n when (n > 30 && n <= 60):
                val = TexttoSpeak + "1'o'clock";
                break;
            case int n when (n > 60 && n <= 90):
                val = TexttoSpeak + "2'o'clock";
                break;
            case int n when (n > 90 && n <= 120):
                val = TexttoSpeak +  "3'o'clock";
                break;
            case int n when (n > 120 && n <= 150):
                val = TexttoSpeak + "4'o'clock";
                break;
            case int n when (n > 150 && n <= 180):
                val = TexttoSpeak + "5'o'clock";
                break;
            case int n when (n > 180 && n <= 210):
                val = TexttoSpeak + "6'o'clock";
                break;
            case int n when (n > 210 && n <= 240):
                val = TexttoSpeak + "7'o'clock";
                break;
            case int n when (n > 240 && n <= 270):
                val = TexttoSpeak + "8'o'clock";
                break;
            case int n when (n > 270 && n <= 300):
                val = TexttoSpeak + "9'o'clock";
                break;
            case int n when (n > 300 && n <= 330):
                val = TexttoSpeak + "10'o'clock";
                break;
            case int n when (n > 330 && n <= 360):
                val = TexttoSpeak + "11'o'clock";
                break;
        }
        return val;
    }
}

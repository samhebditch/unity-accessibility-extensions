﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EventDataStruct;
using Priority_Queue;

public class RCasting : MonoBehaviour
{
    // Setting up objects that will be used later on in the script
    // These will utilise the tags, and other compnents set up and configured
    GlobalConf globConf;
    GameObject castingCube;
    // Strings and variables that can be accessed from  
    // other scripts
    public bool HitStatus; 
    public GameObject hitObj;
    public float DistFloat;
    public bool ObjHasDesc;
    RaycastHit hit; 
    // using this to handle associating objs before any other logic 
    // actually runs
    string ComposedString;
    EventData lastEventData;


    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        globConf = this.gameObject.GetComponent<GlobalConf>();
        castingCube = GameObject.FindWithTag("CastingCube");
    }

    // Update is called once per frame
    void Update()
    {
        if(GlobalVariables.AccessStatus)
        {
            if (globConf.DebugMode)
            {
                RaycastingDbg();
            }
            else
            {
                Raycasting();
            }
        }
        
    }
    void Raycasting()
    {
        if(Physics.Raycast(castingCube.transform.position,transform.TransformDirection(castingCube.transform.forward),out hit, Mathf.Infinity))
        {
            hitObj = hit.collider.gameObject;
            HitStatus = true;
            GlobalVariables.ObjDistance = hit.distance; 
            GlobalVariables.ObjName = hitObj.name;
            // Deciding change of output/result depending on whether the
            // object has a description attached to it. (in this case, I'm going to be
            // handling composing the text that'd normally get passed to the global variables script 
            // and sending it via new event handling method. (for exploration w/ queuing up TTS). 
            if (hitObj.GetComponent<RichText>())
            {
                ObjHasDesc = true;
                ComposedString = "The ray hit " + hitObj.name + ", which was approximately " + (float)Mathf.Round(hit.distance * 100) / 100 + "m away from you. " + hitObj.name + " has a description attached to it, double tap the screen to read it.";
            }
            else
            {
                ObjHasDesc = false;
                ComposedString = "The ray hit " + hitObj.name + ", which was approximately " + (float)Mathf.Round(hit.distance * 100) / 100 + "m away from you. ";
            }

            EventData eventData = new EventData(1, ComposedString);
            if (!string.Equals(lastEventData.data,eventData.data))
            {
                GameObject.FindGameObjectWithTag("ScnCam").SendMessage("Eventhandler", eventData);
                Debug.Log(string.Format("The event data: {0} was sent at {1:00}:{2:00}",eventData.data,Time.time/60,Time.time%60));
            }
            lastEventData = eventData;
            ObjectDescEvent();
        }
        else
        {
            ObjHasDesc = false;
            HitStatus = false; 
            GlobalVariables.ObjName = "Did not hit an object";
        }
    }
    void RaycastingDbg()
    {
        if (Physics.Raycast(castingCube.transform.position, transform.TransformDirection(castingCube.transform.forward), out hit, Mathf.Infinity))
        {              
            hitObj = hit.collider.gameObject;
            HitStatus = true; 
            Debug.Log("A ray just hit: " + hitObj.name);
            Debug.DrawRay(castingCube.transform.position, transform.TransformDirection(castingCube.transform.forward)*hit.distance,Color.green);
            GlobalVariables.ObjDistance = hit.distance;
            GlobalVariables.ObjName = hitObj.name;
            // Deciding change of output/result depending on whether the
            // object has a description attached to it. (in this case, I'm going to be
            // handling composing the text that'd normally get passed to the global variables script 
            // and sending it via new event handling method. (for exploration w/ queuing up TTS). 
            if (hitObj.GetComponent<RichText>())
            {
                ObjHasDesc = true;
                ComposedString = "The ray hit " + hitObj.name + ", which was approximately " + (float)Mathf.Round(hit.distance * 100) / 100 + "m away from you. " + hitObj.name + " has a description attached to it, double tap the screen to read it.";
            }
            else
            {
                ObjHasDesc = false;
                ComposedString = "The ray hit " + hitObj.name + ", which was approximately " + (float)Mathf.Round(hit.distance * 100) / 100 + "m away from you. ";
            }

            EventData eventData = new EventData(1, ComposedString);
            if (!string.Equals(lastEventData.data, eventData.data))
            {
                GameObject.FindGameObjectWithTag("ScnCam").SendMessage("Eventhandler", eventData);
                Debug.Log(string.Format("The event data: {0} was sent at {1:00}:{2:00}", eventData.data, Time.time / 60, Time.time % 60));
            }
            lastEventData = eventData;
            ObjectDescEvent();
        }
        else
        {
            ObjHasDesc = false;
            HitStatus = false;
            Debug.Log("Did not hit an object");
            Debug.DrawRay(castingCube.transform.position, transform.TransformDirection(castingCube.transform.forward) * 1000, Color.red);
            GlobalVariables.ObjName = "did not hit an object";
        }
    }

    // Event handling for reading out the object description 
    void ObjectDescEvent()
    {
        EventData DescEventData = new EventData(3,hitObj.GetComponent<RichText>().ObjectDescription);
        #if UNITY_EDITOR
                if (ObjHasDesc && Input.GetKeyDown(KeyCode.Mouse0))
                {
                GameObject.FindGameObjectWithTag("ScnCam").SendMessage("Eventhandler", DescEventData);
                    
        }

#else
                     for(int i = 0; i < Input.touchCount; i++)
                {
                    if (Input.GetTouch(i).phase == TouchPhase.Began)
                    {
                        if(Input.GetTouch(i).tapCount == 2)
                        {
                            // Double tap input 
                        }
                        if (ObjHasDesc && Input.GetKeyDown(KeyCode.Mouse0))                        
                        {
                            // Single tap input 
                            // which when running with a speech reader, is actually a double tap. 
                            
                                                    GameObject.FindGameObjectWithTag("ScnCam").SendMessage("Eventhandler", DescEventData);
                                                         
                        }
                    }
                }
#endif

    }
}

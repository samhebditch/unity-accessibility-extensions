﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class AccessChecker : MonoBehaviour
{
#if UNITY_IOS
[DllImport ("__Internal")]
private static extern bool _VoiceOverStatus();
   public static bool VOStatus()
    {
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            return (_VoiceOverStatus());        
        }
        else
        {
            return false; 
        }
    }
#endif
}

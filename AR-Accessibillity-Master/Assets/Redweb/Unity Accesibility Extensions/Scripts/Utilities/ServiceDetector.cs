﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class ServiceDetector : MonoBehaviour
{
    public bool AccessStatus()
    {
        // Using platform defs. we can choose whether we want the accessibility detection to run 
        // or not. In this case, if we're running on Android, it'll run the native java code for 
        // detecting the accessibility service, but if we're running and testing in the Editor, we can 
        // toggle the accessibility services via a toggle in the global configuration script. 

#if UNITY_ANDROID && !UNITY_EDITOR
        using (var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                using (var context = activity.Call<AndroidJavaObject>("getApplicationContext"))
                {
                    using (AndroidJavaObject accManager = context.Call<AndroidJavaObject>("getSystemService", new object[] { context.GetStatic<string>("ACCESSIBILITY_SERVICE") }))
                    {
                        
                         if(accManager.Call<bool>("isEnabled"))
                            return accManager.Call<bool>("isTouchExplorationEnabled");
                        
                        return false; 
                        
                        // Existing code used to determine the accessibility service, 
                        // included for legacy purposes and for reference. 
                        //return accManager.Call<bool>("isEnabled");
                        
                    }
                }
            }
        }
#endif
#if UNITY_IOS && !UNITY_EDITOR
        return (AccessChecker.VOStatus());
#endif
#if UNITY_EDITOR
        bool EdStatus; 
    EdStatus = true; 
    return(EdStatus);
#endif
    }
}


﻿using System.Collections;   
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalConf : MonoBehaviour
{
    [Header("Accessibility Service Status")]
    [Header("Unity Accessibility Extensions - Global Conf.")]
    public bool AccessibilityServicesStatus;
    [Header("Enable Accessibility Services (Editor Only)")]
    public  bool EnableAccessibilityServices;
    ServiceDetector serviceDetector;
    [Header("Developer options")]
    public bool DebugMode; 
    // Start is called before the first frame update
     private void Awake() {
    
    }
    void Start()
    {   
        serviceDetector = this.gameObject.GetComponent<ServiceDetector>();
        if (serviceDetector.AccessStatus()) {GlobalVariables.AccessStatus = true;} else {GlobalVariables.AccessStatus = false;}

        if(DebugMode)
        {
            if (GlobalVariables.AccessStatus){ Debug.Log("There are accessibility services enabeld"); }
            else { Debug.Log("Accessibility services are not enabled"); }
        // Putting the debugging messages away into something that is 
        // dependant on the debug mode being enabled. Basically, tidying things up 
        }
    }
        // Update is called once per frame
        void Update()
    {
        
    }

    // These functions are used for the debugging mode option, and are used to toggle it 
    // on and off. 
   public void SetDbugTrue()
    {
        DebugMode = true; 
    }
    public void SetDbugFalse()
    {
        DebugMode = false; 
    }
    #if !UNITY_EDITOR
    private void OnApplicationFocus(bool focus)
    {
        if (focus) 
        {
            if (serviceDetector.AccessStatus()) { GlobalVariables.AccessStatus = true; } else { GlobalVariables.AccessStatus = false; }
        }
    }
    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            if (serviceDetector.AccessStatus()) { GlobalVariables.AccessStatus = true; } else { GlobalVariables.AccessStatus = false; }
        }
    }
    // These functions are used to toggle the accessibilty service status,
    // which will later be used to determine and toggle whether 
    // features and functionality will be enabled, such as raycasting #endregion
    // or otherwise. 
#endif
    // TODO: Change this so it's not only something that happens at runtime, but in 
    // the editor too. 

}

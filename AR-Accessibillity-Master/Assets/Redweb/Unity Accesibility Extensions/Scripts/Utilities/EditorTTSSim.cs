﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorTTSSim : MonoBehaviour
{
    [Range(0, 5)]
    public float DelayRange = 1.5f; //[0.5 - 5] Default 1.5 
    public GameObject tts;
    public static EditorTTSSim instance;
    void Awake()
    {
        instance = this;
        tts = GameObject.FindWithTag("TTS");
    }

    // Setting up some methods which will be called upon, and used to replicate functionality
    // that is handled natively on a iOS or Android Device. 
  public static void EditorConfigure(string language, float _pitch, float _rate)
    {
        Debug.Log("The settings are as follows:" + language  +" "+ _pitch +" " + _rate);
    }
    public static void EditorSpeak(string speakMe)
    {
        Debug.Log(speakMe);
        float DivsVal = instance.DelayRange;
        float NumberOfSecondsToWait = speakMe.Length / DivsVal;
        instance.tts.SendMessage("onStart", "onStart");
        instance.StartCoroutine(DoneSpeaking(NumberOfSecondsToWait));
    }
    public static void EditorStopSpeak() 
    {
        Debug.Log("This will stop the TTS from speaking");    
        // TODO: Implement further functionality here. Not sure how often this is called though. 
    }
    public static void EditorDoneSpeak() 
    {
        Debug.Log("Sending a message back to the TTS script to let it know that everything has finished up");
        instance.tts.SendMessage("onDone","onDone");
        Debug.Log("In theory, we've told the TTS Script that we're done now");
     }
    static IEnumerator DoneSpeaking(float secVal)
    {
        yield return new WaitForSeconds(secVal);
        EditorDoneSpeak();
    }
}

﻿using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class macOSLocaleDetector : MonoBehaviour
{
#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
    // Start is called before the first frame update
    Process localeGetter;
    ProcessStartInfo localeStartInfo;
    public static string localeVal;
    private void Awake()
    {
        determineLocale();
    }
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void determineLocale()
    {
        localeGetter = new Process();
        localeStartInfo = new ProcessStartInfo("/usr/bin/defaults", "read -g AppleLocale")
        {
            RedirectStandardOutput = true,
            CreateNoWindow = true,
            UseShellExecute = false 
        };
        localeGetter.StartInfo = localeStartInfo;
        localeGetter.OutputDataReceived += new DataReceivedEventHandler(processCMD);
        localeGetter.Start();
        localeGetter.BeginOutputReadLine();
    }
    public void processCMD(object target, DataReceivedEventArgs data)
    {
        string ProcessOutput = data.Data;
        if (!string.IsNullOrEmpty(ProcessOutput))
        {
            localeVal = ProcessOutput;
        }
    }
#endif
}

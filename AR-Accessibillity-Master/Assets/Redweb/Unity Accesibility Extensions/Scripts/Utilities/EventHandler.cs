﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventDataStruct;
using Priority_Queue;
using TextSpeech;

namespace EventDataStruct
{

    public struct EventData
    {
        public int prioirty;
        public string data;
        public EventData(int i, string s)
        {
            prioirty = i;
            data = s;
        }
    }
}
public class EventHandler : MonoBehaviour
{
    bool TTSDone;
    bool TTSStart;
    TextToSpeech textToSpeech;
    GameObject GlobalConfig;
    public SimplePriorityQueue<string> eventQueue;
    string lastEvent;
    string nextEvent;
    string eventDequeued;
    EventData lastEventData;
    public List<EventData> eventList;
    // Start is called before the first frame update
    void Start()
    {
        // Setting up TTS and event queue
        GlobalConfig = GameObject.FindGameObjectWithTag("GlobalConf");
        textToSpeech = GlobalConfig.GetComponentInChildren<TextToSpeech>();
        eventQueue = new SimplePriorityQueue<string>();
        // Setting up the events 
        textToSpeech.onDoneCallback += OnTTSDone;
        textToSpeech.onStartCallBack += OnTTSStart;
        eventList = new List<EventData>();
    }

    // Update is called once per frame
    void Update()
    {
        // I've found that the TTS can conflict with the setting script,
        // and as such it could trigger before the floating points were set up
        // correctly 
        if(Time.time > 0)
        {
            if (!TTSDone & !TTSStart)
            {
                if (eventQueue.Count > 0)
                {
                    eventDequeued = eventQueue.Dequeue();
                    //if (!string.Equals(lastEvent, eventDequeued))
                    //{
                        textToSpeech.StartSpeak(eventDequeued);
                        eventList.RemoveAt(0);
                        Debug.Log("Event: " + eventDequeued + "was removed from the eventList");
                    //}
                    lastEvent = eventDequeued;
                }
            }
            else if (TTSStart & !TTSDone)
            {
                Debug.Log("We're not ready to go just yet, there is still speech happening");
            }

            if (TTSDone & TTSStart)
            {
                // Assuming that the TTS has happened, and is now done.
                // Time to reset everything so that it can happen again when called upon. 
                TTSDone = false;
                TTSStart = false;
            }
        }
    }
         public void Eventhandler(EventData eventData)
        {
       
            eventQueue.Enqueue(eventData.data, eventData.prioirty);
            eventList.Add(eventData);
        // Event Queue Pruning
        // We log the previous data, compare it to the current data, if it's the same
        // we remove it. If it's different however, we keep it.
        // Potential to get quite granualar, and add conditions for different priority levels, which would
        // remove the need to have this all handled within the event-sender, and make it easier for people to
        // implement.
            int valToRemove = new int();
            for(var i = 0; i < eventList.Count; i++)
            {
                if (string.Equals(eventData.data, lastEventData.data) && eventData.prioirty != 3)
                // lil' bit hacky, but only doing this comparison if the event's priority is not the one associated with the desc. component
                {
                    Debug.Log("Duplicate entries found at " + i + ". The data for this is: " + eventList[i].data);
                    valToRemove = i;
                    Debug.Log("Duplicate entries logged");
                }
            }
            if (string.Equals(eventData.data, lastEventData.data) && eventData.prioirty != 3)
            {
                eventQueue.Remove(eventList[valToRemove].data);
                Debug.Log("Duplicate entry removed");
            }
        lastEventData = eventData;
    }
       
        void OnTTSDone()
        {
            TTSDone = true;
        }
        void OnTTSStart()
        {
            TTSStart = true;
        }

    } 




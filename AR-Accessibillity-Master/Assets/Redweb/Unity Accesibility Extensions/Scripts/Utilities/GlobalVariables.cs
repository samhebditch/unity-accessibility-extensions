﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;


[System.Serializable]
public class GlobalVariables : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public static bool AccessStatus;
    [SerializeField]
    public static bool EditorAccessStatus = true;  
    public static string ObjName; 
    public static float ObjDistance; 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public static string ItemDescription()
    {
        GameObject GlobalConfObj = new GameObject("GlobalConf1");
        GlobalConfObj.hideFlags = HideFlags.HideInHierarchy;
        if(GameObject.FindGameObjectWithTag("GlobalConf") != null)
        {
                 GlobalConfObj = GameObject.FindGameObjectWithTag("GlobalConf");
        }
        RCasting RCScript = GlobalConfObj.GetComponent<RCasting>();
       if (RCScript != null)
       {
            if(RCScript.HitStatus)
                    {
                        return(RCScript.hitObj.GetComponent<RichText>().ObjectDescription);
                    }
                    else
                    {
                        return "No object description availible.";
                    }
       }
       else{return "Script not found!";} 
    }
    public static string RaycastStatus(){
        GameObject GlobalConfObj = new GameObject("GlobalConf2");
        GlobalConfObj.hideFlags = HideFlags.HideInHierarchy;
        if(GameObject.FindGameObjectWithTag("GlobalConf") != null)
        {
                 GlobalConfObj = GameObject.FindGameObjectWithTag("GlobalConf");
        }
        RCasting RCScript = GlobalConfObj.GetComponent<RCasting>();
        string RCHitStatus;
        string RCMissStatus; 
        if(!RCScript)
        {
         return "cant find the Raycasting Script";
        }
        else if(RCScript){
                if(RCScript.HitStatus)  
                {
                    RCMissStatus = string.Empty;
                if (RCScript.ObjHasDesc)
                {
                    RCHitStatus = "The ray hit " + GlobalVariables.ObjName + ", which was approximately " + (float)Mathf.Round(GlobalVariables.ObjDistance * 100) / 100 + "m away from you. " + GlobalVariables.ObjName + " has a description attached to it, double tap the screen to read it.";
                }
                else 
                RCHitStatus = "The ray hit " + GlobalVariables.ObjName + ", which was approximately " + (float)Mathf.Round(GlobalVariables.ObjDistance *100)/100+ "m away from you";
                    return RCHitStatus;
                }
                else if(!RCScript.HitStatus)
                {
                    RCHitStatus = string.Empty;
                    RCMissStatus = "The ray did not hit an object";
                    return RCMissStatus;
                }
                else{return "Not getting any status from the script";}
        }
        else{return "something isn't quite working";}

    }
}

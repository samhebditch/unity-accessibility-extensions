﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class LocaleDetector : MonoBehaviour
{
    #if UNITY_IOS
        [DllImport ("__Internal")]
    private static extern string LocaleVal();
    public static string LocaleValue()
    {
            if(Application.platform == RuntimePlatform.IPhonePlayer)
            {
            return (LocaleVal());
        }
        else
        {
            return "This isn't working";
        }
    }
#endif
}

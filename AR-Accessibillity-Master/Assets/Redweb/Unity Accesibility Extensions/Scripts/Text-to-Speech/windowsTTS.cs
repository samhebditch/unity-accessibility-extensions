﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using UnityEngine.Windows.Speech;
using System.Runtime.InteropServices;
public class windowsTTS : MonoBehaviour
{
#if FAKE_WINDOWS_VOICE
    public class WindowsVoice : MonoBehaviour
    {
      public TextMeshProUGUI DebugOutput = null;

      public static WindowsVoice theVoice = null;
      void OnEnable () 
      {
        if (theVoice == null)
          theVoice = this;
      }
      public static void speak(string msg, float delay = 0f) {
        if (Timeline.theTimeline.QReprocessingEvents)
          return;

        if (delay == 0f)
        {
          if (theVoice.DebugOutput != null)
            theVoice.DebugOutput.text = msg;
          else
            Debug.Log("SPEAK: " + msg);
        }
        else
          theVoice.ExecuteLater(delay, () => speak(msg));
      }
    }
#endif
    public bool DebugMsgs;
    [DllImport("WindowsVoice")]
    public static extern void initSpeech();
    [DllImport("WindowsVoice")]
    public static extern void destroySpeech();
    [DllImport("WindowsVoice")]
    public static extern void addToSpeechQueue(string s);
    [DllImport("WindowsVoice")]
    public static extern void clearSpeechQueue();
    [DllImport("WindowsVoice")]
    public static extern void statusMessage(StringBuilder str, int length);
    //public static WindowsVoice theVoice = null;

    private void OnEnable()
    {
        initSpeech();
        if (DebugMsgs)
        {
            Debug.Log("init complete");
        }
    }
    public void StartSpeak(string txt)
    {
        addToSpeechQueue(txt);
        if (DebugMsgs)
        {
            Debug.Log("Speaking: " + txt);
        }
    }
    public void StopSpeak()
    {
        destroySpeech();
        if (DebugMsgs)
        {
            Debug.Log("Done speaking");
        }
    }

}

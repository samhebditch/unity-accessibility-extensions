﻿using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using UnityEngine;
using TextSpeech;
using Debug = UnityEngine.Debug;

public class TTS : MonoBehaviour
{
    GameObject GlobalConfig;
    TextToSpeech tts;
    public bool TTSStart;
    public bool TTSDone;
    public bool TTSDescDone;
    string LastSpokenString;

    // Locale detection, used to set the TTS engine's accent/pronounciations. 

    public static string LocaleValue()
    {
#if UNITY_IOS && !UNITY_EDITOR
        return LocaleDetector.LocaleValue();
#endif
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        // returning values on Windows. Returns current culture used for UI. 
        // e.g. en_gb
        return System.Globalization.CultureInfo.CurrentUICulture.Name;
#endif

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        // This queries the default macOS functionality for returning locale. 
        return macOSLocaleDetector.localeVal;
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
        {
                using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
                {
                    if (locale != null)
                    {
                        string localeVal;
                        localeVal = locale.Call<string>("getLanguage") + "_" + locale.Call<string>("getCountry");
                        return localeVal;
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
        }
#endif
    }
    // Start is called before the first frame update
    void Start()
    {
        GlobalConfig = GameObject.FindGameObjectWithTag("GlobalConf");
        tts = GlobalConfig.GetComponentInChildren<TextToSpeech>();

        // Set up locale as needed, as some text to speech engines don't 
        // really like the default of en-US for some reason and default to 
        // Chinese, Japanese, or something for some bizzare logic-jump/reason
        // (for example, Google TTS.. thanks Google. 🙄 )

        tts.Setting(LocaleValue(), 1f, 1f);
        tts.onDoneCallback += OnTTSDone;
        tts.onStartCallBack += OnTTSStart;
    }

    // Update is called once per frame
    void Update()
    {
        if (TTSDone & TTSStart)                                                                                                                                                                                                                      
        {
            // Assuming that the TTS has happened, and is now done.
            // Time to reset everything so that it can happen again when called upon. 
            TTSDone = false;
            TTSStart = false;
        }
    }
    public void SpeakOnHit()
    {
        // We assume that if these are both set to false, that either, it's the first time the 
        // application is being run, or that they've been reset after the speech has completed
        // and now we're ready to again, without flooding the TTS system with numerous requests. 
        if (GlobalVariables.RaycastStatus() != LastSpokenString)
        {
            if (!TTSDone & !TTSStart)
            {
                string ObjName = GlobalConfig.GetComponent<RCasting>().hitObj.name;
                tts.StartSpeak(GlobalVariables.RaycastStatus());
                LastSpokenString = GlobalVariables.RaycastStatus();
            }
            else if (TTSStart & !TTSDone)
            {
                // Debugging/exception handling, basically 
                // We don't want the TTS to run, whilst speaking, as that produces no result/overflows/errors.
                Debug.Log("We're not ready to go just yet, there is still speech happening");
            }
        }
    }
    void OnTTSDone()
    {
        TTSDone = true;
    }
    void OnTTSStart()
    {
        TTSStart = true;
    } 
    public void TTSSpeakDesc()
    {
        if(!TTSDone & !TTSStart)    
        {

#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                tts.StartSpeak(GlobalConfig.GetComponent<RCasting>().hitObj.GetComponent<RichText>().ObjectDescription);
            }
#else
             for(int i = 0; i < Input.touchCount; i++)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                if(Input.GetTouch(i).tapCount == 2)
                {
                    // Double tap input 
                }
                if(Input.GetTouch(i).tapCount == 1)
                {
                    // Single tap input 
                    tts.StartSpeak(GlobalConfig.GetComponent<RCasting>().hitObj.GetComponent<RichText>().ObjectDescription);
            }
            }
        }
#endif
        }
        else if(TTSStart & !TTSDone)
        {
            Debug.Log("We're not quite ready to go, as the TTS engine is still speaking");
        }
    }
}

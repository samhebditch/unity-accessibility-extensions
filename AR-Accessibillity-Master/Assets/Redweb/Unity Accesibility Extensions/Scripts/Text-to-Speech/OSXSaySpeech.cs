﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TextSpeech;
using System.Diagnostics;
using System.IO;

public class OSXSaySpeech : MonoBehaviour
{
    TextSpeech.TextToSpeech tts;
    Process speechProcess;
    Process speechPopulator;
    bool wasSpeaking;
    public string voice;
    // these values are pulled from the TTS script, and adjusting them there will update the
    // values here
    public float rate;
    public float CorrectedRate;
    public float pitch;
    public float CorrectedPitch;
    // Speech Population 
    public Process langDetective;
    public ProcessStartInfo langDetectiveObjectives;
    public static string[] SpeechNameArray;
    public List<string> speechNames;


    private void Awake()
    {
        //voice = "Samantha";
        tts = this.gameObject.GetComponent<TextToSpeech>();
        //InstalledVoiceDetective();
    }

    private void Update()
    {
        bool isSpeaking = (speechProcess != null && !speechProcess.HasExited);
        if (isSpeaking != wasSpeaking)
        {
            if (isSpeaking) tts.onStart("message") ;
            else tts.onDone("message");
            wasSpeaking = isSpeaking;
        }
    }
    public void Speak(string text)
    {
        string cmdArgs = string.Format(" \"[[pbas" + CorrectedPitch +"]] {0}\" -r {1}", text.Replace("\"", ","),CorrectedRate);
        speechProcess = System.Diagnostics.Process.Start("/usr/bin/say", cmdArgs);
        UnityEngine.Debug.LogWarning("The current arguments being passed to the TTS are" + cmdArgs + string.Format(" which is happening at {0}",string.Format("{0:00}:{1:00}",Time.time/60,Time.time%60)),this.gameObject);
        UnityEngine.Debug.LogWarning(string.Format("The raw values for pitch and speech rate are as folows; P: {0} R: {1}",pitch,rate));
    }
    public void StopSpeak()
    {
        speechProcess.Close();
    }
    public void SetupSpeech(float p, float r)
    {
        pitch = p;
        rate = r;
        CorrectedRate = rate * 200;
        CorrectedPitch = pitch * 100;
    }

    public void InstalledVoiceDetective()
    {
        //SpeechNameArray = new string[speechNames.Count];
        langDetective = new Process();
        langDetectiveObjectives = new ProcessStartInfo("/usr/bin/say", "-v '?' ")
        {
            RedirectStandardOutput = true,
            CreateNoWindow = true,
            UseShellExecute = false
        };
        langDetective.StartInfo = langDetectiveObjectives;
        langDetective.OutputDataReceived += new DataReceivedEventHandler(InstalledVoiceDataProcessor);
        langDetective.Start();
        langDetective.BeginOutputReadLine();
    }
    public void InstalledVoiceDataProcessor(object target, DataReceivedEventArgs d)
    {
        string cmdOutput = d.Data;
        using (StringReader reader = new StringReader(cmdOutput))
        {
            string line = string.Empty;
            do
            {
                line = reader.ReadLine();
                if (line != null)
                {
                    string FirstWord = line.Split(' ')[0];
                    //UnityEngine.Debug.Log(FirstWord);
                    speechNames.Add(FirstWord);
                    SpeechNameArray = speechNames.ToArray();
                }

                // This was an experiment to see what happens 
                // if I try and assign to the array myself. 
                // I think the corruption might happen when I try and change the 
                // list to an array. Doing that multiple times, after the initial read might
                // break things 
                //if(line != null)
                //{
                //    i++;
                //    UnityEngine.Debug.Log(i);
                //    SpeechNameArray = new string[i];
                //    string FirstWord = line.Split(' ')[0];
                //    SpeechNameArray[i] = FirstWord;
                //}
            } while (line != null);
        }
    }
}

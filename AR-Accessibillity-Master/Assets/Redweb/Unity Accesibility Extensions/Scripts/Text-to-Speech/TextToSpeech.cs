﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System;

namespace TextSpeech
{
    public class TextToSpeech : MonoBehaviour
    {

        #region Init
        windowsTTS winTTS;
        OSXSaySpeech osxSaySpeech;
        WindowsVoice winVoice; 
        static TextToSpeech _instance;
        public static TextToSpeech instance
        {
            get
            {
                if (_instance == null)
                {
                    Init();
                }
                return _instance;
            }
        }
        public static void Init()
        {
            if (instance != null) return;
            GameObject obj = new GameObject();
            obj.name = "TextToSpeech";
            _instance = obj.AddComponent<TextToSpeech>();
        }
        void Awake()
        {
            winTTS = this.gameObject.GetComponent<windowsTTS>();
            osxSaySpeech = this.gameObject.GetComponent<OSXSaySpeech>();
            winVoice = this.gameObject.GetComponent<WindowsVoice>();
            _instance = this;
            Debug.Log("Script is awake");
        }
        #endregion

        public Action onStartCallBack;
        public Action onDoneCallback;
        public Action<string> onSpeakRangeCallback;

        // //[Range(0.5f, 2)]
        //public float pitch; //[0.5 - 2] Default 1.5
        // [Range(0.5f, 2)]
        //public float rate; //[min - max] android:[0.5 - 2] iOS:[0 - 1]
        [SerializeField]
        public float pitch;
        [SerializeField]
        public float rate; 

        public void Setting(string language, float _pitch, float _rate)
        {
            pitch = _pitch;
            rate = _rate;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            
#elif UNITY__STANDALONE_OSX || UNITY_EDITOR_OSX
            osxSaySpeech.SetupSpeech(pitch, rate);
            Debug.Log(string.Format("Setting the pitch at {0} and the rate at {1}",pitch,rate));
#elif UNITY_EDITOR
            EditorTTSSim.EditorConfigure(language, pitch, rate);
#elif UNITY_IPHONE
        _TAG_SettingSpeak(language, pitch, rate / 2);
#elif UNITY_ANDROID
        AndroidJavaClass javaUnityClass = new AndroidJavaClass("com.starseed.speechtotext.Bridge");
        javaUnityClass.CallStatic("SettingTextToSpeed", language, pitch, rate);
#endif
            Debug.Log(string.Format("Voice setup is done at approx. {0:00}:{1:00}", Time.time / 60, Time.time % 60));
        }
        public void StartSpeak(string _message)
        {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            winVoice.StartSpeak(_message);
#elif UNITY__STANDALONE_OSX || UNITY_EDITOR_OSX
            //EditorTTSSim.EditorSpeak(_message);
            //_TAG_StartSpeaking(_message);
            osxSaySpeech.Speak(_message);
#elif UNITY_EDITOR
            EditorTTSSim.EditorSpeak(_message);
#elif UNITY_IPHONE
        _TAG_StartSpeak(_message);
#elif UNITY_ANDROID
            AndroidJavaClass javaUnityClass = new AndroidJavaClass("com.starseed.speechtotext.Bridge");
        javaUnityClass.CallStatic("OpenTextToSpeed", _message);
#endif
        }
        public void StopSpeak()
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            winVoice.OnDestroy();
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            //_TAG_StopSpeaking();
            osxSaySpeech.StopSpeak();
#elif UNITY_EDITOR
            EditorTTSSim.EditorStopSpeak();
#elif UNITY_IPHONE
        _TAG_StopSpeak();
#elif UNITY_ANDROID
        AndroidJavaClass javaUnityClass = new AndroidJavaClass("com.starseed.speechtotext.Bridge");
        javaUnityClass.CallStatic("StopTextToSpeed");
#endif
        }
        public void onSpeechRange(string _message)
        {
            if (onSpeakRangeCallback != null && _message != null)
            {
                onSpeakRangeCallback(_message);
            }
        }
        public void onStart(string _message)
        {
            if (onStartCallBack != null)
                onStartCallBack();
        }
        public void onDone(string _message)
        {
            if (onDoneCallback != null)
                onDoneCallback();
        }
        public void onError(string _message)
        {
        }
        public void onMessage(string _message)
        {

        }
        /** Denotes the language is available for the language by the locale, but not the country and variant. */
        public const int LANG_AVAILABLE = 0;
        /** Denotes the language data is missing. */
        public const int LANG_MISSING_DATA = -1;
        /** Denotes the language is not supported. */
        public const int LANG_NOT_SUPPORTED = -2;
        public void onSettingResult(string _params)
        {
            int _error = int.Parse(_params);
            string _message = "";
            if (_error == LANG_MISSING_DATA || _error == LANG_NOT_SUPPORTED)
            {
                _message = "This Language is not supported";
            }
            else
            {
                _message = "This Language valid";
            }
            Debug.Log(_message);
        }
        private void Update()
        {
            Debug.Log("pitch: " + pitch.ToString() + "rate: " + rate.ToString());
        }

#if UNITY_IPHONE
        [DllImport("__Internal")]
        private static extern void _TAG_StartSpeak(string _message);

        [DllImport("__Internal")]
        private static extern void _TAG_SettingSpeak(string _language, float _pitch, float _rate);

        [DllImport("__Internal")]
        private static extern void _TAG_StopSpeak();
#endif
#if UNITY_STANDALONE_OSX  || UNITY_EDITOR_OSX
        //[DllImport("UnityTextToSpeech")]
        //private static extern void _TAG_StartSpeaking(string _message);
        //[DllImport("UnityTextToSpeech")]
        //private static extern bool  _currentlySpeaking();
        //[DllImport("UnityTextToSpeech")]
        //private static extern void _TAG_StopSpeaking();
#endif

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChngColour : MonoBehaviour
{
    GameObject GlobalConf;
    ServiceDetector serviceDetector;

    // Start is called before the first frame update
    void Start()
    {
        GlobalConf = GameObject.FindGameObjectWithTag("GlobalConf");
        serviceDetector = GlobalConf.GetComponent<ServiceDetector>();
        if (serviceDetector.AccessStatus()) 
        {
            this.GetComponent<Renderer>().material.color = Color.green;
        }
        else { this.GetComponent<Renderer>().material.color = Color.red; }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void ApplicationFocus(bool focus)
    {
        if (focus)
        {
            GlobalConf = GameObject.FindGameObjectWithTag("GlobalConf");
            serviceDetector = GlobalConf.GetComponent<ServiceDetector>();
            if (serviceDetector.AccessStatus())
            {
                this.GetComponent<Renderer>().material.color = Color.green;
            }
            else { this.GetComponent<Renderer>().material.color = Color.red; }
        }

    }
    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            GlobalConf = GameObject.FindGameObjectWithTag("GlobalConf");
            serviceDetector = GlobalConf.GetComponent<ServiceDetector>();
            if (serviceDetector.AccessStatus())
            {
                this.GetComponent<Renderer>().material.color = Color.green;
            }
            else { this.GetComponent<Renderer>().material.color = Color.red; }
        }
    }

}


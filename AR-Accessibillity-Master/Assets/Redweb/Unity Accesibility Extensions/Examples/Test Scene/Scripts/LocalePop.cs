﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LocalePop : MonoBehaviour
{
    public TextMeshProUGUI textMeshProUGUI;
    // Start is called before the first frame update
    void Start()
    {
        textMeshProUGUI.text = TTS.LocaleValue();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

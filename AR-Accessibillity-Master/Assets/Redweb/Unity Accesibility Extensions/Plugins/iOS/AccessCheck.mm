
extern "C"
{
    bool _VoiceOverStatus()
    {
        return UIAccessibilityIsVoiceOverRunning();
    }
}
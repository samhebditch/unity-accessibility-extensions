extern "C"
{
    char* cStringCopy(const char* string)
    {
        if (string == NULL)
            return NULL;
        
        char* res = (char*)malloc(strlen(string) + 1);
        strcpy(res, string);
        
        return res;
    }

    char*  LocaleVal()
    {
        NSString *LocaleV = [[NSLocale currentLocale] localeIdentifier];
        return cStringCopy([LocaleV UTF8String]);
    }
}

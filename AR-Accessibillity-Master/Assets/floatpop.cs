﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using TextSpeech;

public class floatpop : MonoBehaviour
{
    TextMeshProUGUI textMeshPro;
    // Start is called before the first frame update
    void Start()
    {
        textMeshPro = this.gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        textMeshPro.text = GameObject.FindGameObjectWithTag("TTS").GetComponent<TextToSpeech>().pitch.ToString();
    }
}

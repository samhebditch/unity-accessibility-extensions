Installation Process
--------------------

As mentioned on the rationale page, the aim for these extensions was to be able to easily set up and configure these extensions with little to no developer input (reconfiguration of scenes, refactoring code, etc.) As such, the installation process is as automated as possible, nonetheless, it's worth documenting it, including current quirks and oddities when it comes to configuring the extensions at the moment. 

Initial Setup 
~~~~~~~~~~~~~

Initial set up is relatively easy, download the latest package from the releases page, and import it as you would any other custom Unity package.

.. figure:: /images/import.png
   :align: center
   :alt: A screenshot depeicting the menus needed to go through to import custom packages. 
   :figclass: align-center
   :width: 35%

   A screenshot depeicting the menus needed to go through to import custom packages. 

Once this is done, you'll notice a new item appear within your Unity menu bar, much like the above screenshot. To start the initial setup, click *placeholder*. A simple dialog will appear that gives you options for configuring the components required in the script, along with the status of each of these components. 

.. figure:: /images/menuitem.png
   :align: center
   :alt: The menu items that appear once the package import is complete
   :figclass: align-center
   :width: 35%

   The menu items that appear once the package import is complete


There is a two step process to setting up the extensions, handily outlined within the status window. We firstly set up the global configuration object, which handles all of the main scripts, before re-tagging the camera and adding in our raycasting source object.

|beforesetup|        |aftersetup|

.. |beforesetup| image:: /images/setup.png
   :width: 35%
   :alt: The setup processs before completing the steps 

.. |aftersetup| image:: /images/setupdone.png
   :width: 35%
   :alt: The setup process after completing the steps


The above images showcase the setup dialog before, and after the setup process has been completed.

Additional Options 
~~~~~~~~~~~~~~~~~~

There are some additional options contained within the initial setup window, below is a brief outline and explanation of these.

- Debug Mode:
    This is mode enables debugging messaging, and other information to be visible when working within the editor. It will also enable debugging 
    lines when working with Raycasting in the editor, alowing you to view the directions that the raycast is going in, whether it's intersecting 
    or hitting an object, or casting infinitely into the distance. 


- Custom Camera Tag
    Currently, the script will create a new tag called :code:`ScnCam` and apply this to camera, these options can circumvent this, and disable this functionality, 
    instead allowing the user to use the default Unity camera tag, or a completely bespoke camera tag, allowing for integration (as some render pipelines
    might be dependant on custom tags etc.) 




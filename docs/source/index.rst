Welcome to the Unity Accessibility Extensions (UAE) documentation!
==================================================================
This set of documents is designed to outline and explain the principles
behind the creation of the Unity Accessibility Extensions, alongside documentation
of the code and underlying technologies that enable this functionality.

These extensions are designed to aid visually impaired or blind people access Augmented, Virtual and Mixed Reality
experiecnes that have been created within Unity. One of the design goals for these extensions, along with making experiecnes 
accessible, was to require as little input, or initial bring up to encourage developers to implement them. Unity itself, is slowly 
making strides towards accessibility, with a new accessibility and inclusivity focus and thread being set up on their community forums. 
however, it's currently very lacking  in featuers to make games and experiences created within Unity accessible. 

These tools make use of native operating system APIs, as outlined within the "Native Overview" page to provide native fucntionality 
to aid the user. 

I hope that these extensions prove a useful starting point, resouce, or reference and when paired with this documentation, can provide you
with a good idea of the methodology and process behind making an experience accesible for those who are visually impaired or blind. 

Sam Hebditch. 




.. toctree::
   :maxdepth: 2
   :caption: Contents:


   introduction/introduction
   rationale
   installation
   NativeOverview
   usefullinks

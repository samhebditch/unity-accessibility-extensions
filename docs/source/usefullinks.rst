Useful Links and Resources
==========================

Starting out this project, I didn't find much in the way of useful links and resources 
for making Unity experiences accessible, yet alone making any kind of XR experience accessible. 

Since starting this journey, however, I've found several links, tools, resources and references that 
may prove useful for someone diving into this, or curious about this! 

Communities
~~~~~~~~~~~

- Unity: Accessibility and Inclusivity Thread
    `https://forum.unity.com/threads/accessibility-and-inclusion.694477/ <https://forum.unity.com/threads/accessibility-and-inclusion.694477//>`_

    Currently a bit dead, but a worthwhile insight that I'm hoping will get updated as the Unity team implement
    and improve upon accessiblity features within the editor, and at the API/engine level. 

- XR Access Initiative
    `https://www.xraccess.org/ <https://www.xraccess.org/>`_

    A community focused around making AR, VR and XR accessibile to those with visual impairements. Offers a wide variety of links 
    to tools, guidelines, and players within the XR accesibility space.

- W3C Immersive Web Working Group
    `https://www.w3.org/immersive-web/ <https://www.w3.org/immersive-web/>`_


    Whilst this a community centered around immersive web technologies, such as WebXR, there is some crossover as the group begin to explore
    how current web APIs, or future APIs can be used to make these accessible. In November 2019, the W3C are planning on hosting an inclusive 
    design workshop for the immersive web, which should produce some interesting results for those looking to eplore web based immserive tech, links 
    to that can be found `here <https://www.w3.org/2019/08/inclusive-xr-workshop/>`_.


Tools and Resources
~~~~~~~~~~~~~~~~~~~
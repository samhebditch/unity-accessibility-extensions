#!/bin/bash
echo Simple CI for Unity Documentation hosted on Read the Docs 
# This command gives us the build status as a value 
# curl --silent https://readthedocs.org/api/v2/build/?commit=$(git log -n 1 --pretty=format:"%H") | grep -o '"state":..........'

# Strings that will be checked against
finstrin='"state":"finished"'
installstrin='"state":"installing"'

# populating hash val from latest commit 
githash=$(git log -n 1 --pretty=format:"%H")
echo Latest commit is $githash

apistate=$(curl --silent https://readthedocs.org/api/v2/build/?commit=$githash?)
buildstate="$(curl --silent https://readthedocs.org/api/v2/build/?commit=$(git log -n 1 --pretty=format:"%H") | grep -o '"state":...........')"

while [[ "$buildstate" =~ !"$finstrin" ]]; do 
echo Current build status is $buildstate
sleep 10
done

if [[ "$buildstate" =~ "$finstrin" ]]; then 
    echo ✅ Documentation build finished successfully! 
elif [[ "$buildstate" =~ "$installstrin" ]]; then
    echo ⏰ Documentation is still building and installing..
fi 
# Unity Accessibility Extensions 
These are a set of scripts, tools, and features that aim to make applications and games developed within Unity accessible to users that have a visual impairment, or are fully blind. The original goal behind this project was to explore how Augmented Reality could be made accessible, however, the underlying principles and technologies can be adapted towards any 3D Unity application. 

### As of 30th May 2019, ExperimentalEventDelegation is fully functional, and I've deemed it worthy to merge it back into master.

I will also be redirecting ReadTheDocs to pull from the master branch now, as this includes all of the latest documentation. 


## How does this work exactly? What features does this offer
**TODO**: Write-up features and functionality, and everything that is offered by the tools that I’ve developed

## How do I implement this?
**TODO**: Cover implementation process 

## Other information 
**TODO**: Include contact information for yourself, other information
